# This is meant to be a brief overview of the project for contributors and players. 
Detailed explanations of the mechanics can be found in [crunch.md](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md), elaboration on lore can be found in [fluff.md](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/fluff.md)

Interrupted by Dmitry, will finish later

---

## Where's the Fun?

Apparently the first and foremost thing to consider when designing a game, is to locate where the fun is and how to maximize on it. After interviewing some mates, and reflecting on my own experiences with the sort of fun that I'm trying to cultivate I've come up with a few vectors of fun to focus on.

Throughout the course of development these will be our foci with the hopes of maximizing fun.

|Fun Element|Notes|
|---|---|
|Dungeoneering|Having to navigate environmental hazards, monsters, different layouts, and kill bosses makes exploring dungeons a fun thing to do. So we'll aim to make exploring dungeons as fun as possible.|
|Tactics|Having to use your brain to survive encounters with powerful monsters, or navigate between several monsters and environmental hazards. Exploiting things like elemental or thematic strengths/weaknesses, or whatever bullshit you happen to have access to at that particular instance. Additionally, bosses will have mechanics. Depending on the strength of your character you may or may not be able to get away with not respecting the mechanics, but they will be there so that players of all playstyles can have a fighting chance against bosses.|
|Silliness|Anything can happen, and often does. The random nature of life in the Zone and general wackiness of shit that could happen, which almost makes sense but not quite can often be a source of great entertainment.|
|Feeling of Progression|Numbers getting bigger, and the feeling of progress when you get a strong new piece of equipment, or manage to get a skill level to certain height that you've been aiming for.|
|[Pets and pet management](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md#pets)|Being able to interact with and control your companions is intended to be a major source of fun and as such will be one of the things we focus on.|

## Abstract - Game Experience 

### Character Generation

The following table will outline the chargen process in the order that things happen.

|Process|Notes|
|---|---|
|Name|Your character's name, we should probably have a randomize feature here|
|Callsign/Alias|For meme potential and an homage to Elona. We'll likely use this when broadcasting deaths/achievements|
|Bundle|You can choose whether or not you want to use a "bundle" which predetermines all of your character's starting stats/skills/feats/equipment/pets/etc. and would skip the rest of the this process up until character customization and then dump you in game.|
|Race|Race will affect innates and stat/skill bonuses, will possibly also confer environmental advantages|
|Class|Class will affect innates and stat/skill bonuses, and possibly have unique inherent feats/spacts|
|Stats|You'll be able to delegate your starting stat points here. You will have the option of either using point buy, or selecting from a number of pre-rolled arrays.|
|Skills|You'll be able to delegate your starting skill points here.|
|Feats|You'll be able to choose your starting feats here. Perhaps some will only be available to lvl 1 players.|
|Religion|Your character's deity of choice, you start as a practitioner of their religion and have enough piety with that deity to get a starting pet|
|Psych profile|Your character's psychological profile (parental relationships, big 5 personality traits, fears, political orientation) will affect the generation of certain NPCs (pets, allies, etc.), bosses, and enemies. Additionally, it will also affect the layout of the Tower|
|Character Customization|Depending on how many assets we have, you might be able to customize your character's sprite, or 'upload' your own|
|Backstory|Your character's backstory is completely arbitrary. It can be automatically generated or written by the player.|

### Gameplay, interacting with the world

The game will take place from a top down perspective consistent with roguelike conventions. The user interface will consist of the typical status bars as well as the currently loaded map. 

All monsters/npcs will be similar to the player character insofar as the sort of objects they are. Consequently they'll all have the same type of stats, inventories, status effects, etc. Examples of monsters can be found in the Fluffdoc.

Generally speaking, [interacting](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md#interacting-with-the-environment-inside-of-dungeonstownsmaps) with dungeons and environmental hazards or objects is not much different from in Angband, ADOM, or Elona. You can find a comprehensive list of possible interactions [here](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md#interacting-with-the-environment-inside-of-dungeonstownsmaps)

There will be a world map broken into multiple regions representing different countries or landmasses.

On the world map there will be 5 types of locations:

1. Towns/cities, which contain questgivers, shops, and plot. These are fixed and don't appear/disappear (except unless we require it in the story, but that's unlikely)
2. Story dungeons, which exist to provide static artifacts or aid with game progression. The state of these remains the same unless you acquire an artifact that allows you to reset them at the cost of making them more difficult.
3. Conditional towns/dungeons, which appear when certain pre-requisites or environmental conditions are met (weather, moon phase, date, achievements, etc.)
4. Randomly generated dungeons, which will appear and disappear after a certain amount of time
5. Player owned locations, such as farms, player base, etc.

In addition to those locations, there will also be random encounters/events as you travel. Random events will also occur while the player character is actually inside of a location and not on the world map, but those will be drawn from a different pool. You can find the pools in question [here in the Crunchdoc](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md)

### Graphics

We'll likely just be using sprites/tiles. It's possible that if there's enough interest internally that we'll crib DF's idea and make tilesheets using unicode characters and some png magic.

### Maps / Locations 

Dungeons will all have a danger level, a type, and a modifier/mood. The danger level will determine the tier/relative strength of the enemies, the type will determine the tileset and shape/layout, and the pool of monsters and loot that appears in the dungeon as well as the frequency/type of traps. The modifier/mood will have random effects on the dungeons based on which one you get. The harder the dungeon, the more modifiers, but more/better loot.

On that note, the basic things that you'll find in every dungeon/map:

|Things in dungeons|Notes|
|---|---|
|Monsters|Monsters can be passive or non-aggro, most will not be.|
|Traps|You can find a comprehensive list of traps in the crunchdoc|
|Loot |This includes chests or containers, some of which may be locked, or require you to kill certain things to open them|
|Resources to gather|Resources can be used in crafting, or summoning pets or artifacts.|
|Stairs/ladders|These are used to ascend/descend dungeon levels. It's possible that there may be dungeons that have different sub-dungeons depending on where you ascend/descend. Kind of like the Vaal areas in PoE|
|A boss|Plot dungeons will have mechanics, randomly generated dungeons might also feature boss mechanics if we can figure out how to generate them. Otherwise, bosses just drop phat loot and certain things common to all bosses.|

Something I've been considering is racial (or trait based) advantages/disadvantages based on terrain. So like a merfolk might be better adapted to an aqueous environment. Or Elves might thrive in the forest. Stuff like that. I'm also very interested in weather affecting outdoor maps. 

### The Player Character

Re-hashing what was said above about the player in the chargen section, the PC will basically consist of the following things:

|Thing|Description|Notes|
|---|---|---|
|Name|Self explanatory|Might use this for global broadcasts about people accomplishing things or dying|
|Alias/Callsign|Will be used as a sobriquet when referring to a given player|Might use this for global broadcasts about people accomplishing things or dying|
|Race|The character's race, this will mostly affect things like vision/line-of-sight low-light vision, starting bonuses to skills, terrain/weather advantages, etc.|This will probably be unchangable, but maybe at some point you can fuse or alter yourself|
|Class|Same as race, but I've been thinking about some of the mechanics|I've been considering having this be like certain FF/DQ games where you can change class throughout the course of the game and once you master a skill you can use it while any class. It's either that, or just having Class set in stone and unchangable, but you only get like 1 special trait and a few special actions or feats from it and everything else is universal. If we do go the "you can change classes" route, there may be unlockable prestige classes. Alternatively, progressing through the game as a given class might unlock upgraded versions of the class with higher innate stats/skills/potentials for later playthroughs.|
|Religion|This will function a lot like religion in Elona, where based on your piety or relationship with your god you earn rewards, and gain stat benefits or a trait|The main difference from Elona, is that I was thinking you could start as the practicioner of a religion of your choice at the start of the game to save you some time. Additionally, you may get an extra starting pet based on your deity of choice. Maybe if you play as a priest you start with extra piety, lol.|
|Skills|Skills will basically affect everything that stats do not. You can break down "things that affect actions and their calculations" into four categories. Simple stats, complex stats, equipment, and skills.|Not all skills will be available by default, your starting skills will be based on your race and class, and then you'll have to learn the rest of them later. Not sure if there should be class exclusive skills or not, probably not.|
|Special Actions|Will be unlocked based on level/stat/skill requirements, some will be class related, most will not. They will be usable actions/skills that consume HP/MP or STA (if we want a stamina system)|Also called 'spacts'|
|Other Considerations|I've been thinking about hunger, thirst, nutrition as possible things to include.|Hunger at the very least, is a given.|

### Monster and Pets

The idea is that all living entities in the game will have the same sort of stats/inventory/skills/feats/races/classes as player characters, and you can interact with them the same. Once you tame or otherwise add an entity to your party, you unlock more ways to interact with them based on your synergy and affection. You can find more details on this [here](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md#pets)

### Combat

The following things will determine a character's effectiveness in combat

|Character Components|Notes|
|---|---|
|Stats|Stats will affect things such as a character's HP/MP/Stamina reserves, as well as their inherent unarmed hit dice, inherent AC/DC/defense, etc.|
|Skills|Skills will affect things such as your hitdice for weapons, damage multipliers, and everything else. Ultimately contributing to how damage/hit calculation is done in a fight|
|Feats|Feats might affect calculation in interesting or novel ways giving variery to a character's build|
|Equipment|The type of weapon determines what skills are used for hit/damage calculations, as well as the actual dice used. All of your equipment contributes to resists, AC/DC, and can possibly increase stats and skills as well as having inherent or intrinsic feats. Some of these will be unique to artifacts or randarts.|

You can find more details about these things in the [Crunchdoc](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md).

The following things will be the type of attacks used 

|Type of Attack|Notes|
|---|---|
|Melee|Melee attacks use the hitdice and damagedice of physical weapons to hit another entity. The first calculation is hit vs dodge, if the enemy fails to dodge the attack, then damage is calculated. When damage is calculated, resists and defense decrease the amount of total damage. Depending on a weapon's 'pierce' attribute, a certain % of damage can ignore damage reduction.|
|Ranged|Ranged attacks use the hitdice and damagedice of their weapons to use a missile to hit other entities. The first calculation is shot difficulty, which affects the hitdice negatively or positively based on distance. The second is hit vs dodge. And then finally, damage calculation. Missiles can also have pierce.|
|Spells|Spells never miss, but some entities may have the ability to dodge, absorb, or reflect them. The only calculations involved with spells are damage or efficacy (for effects that don't cause damage or restore hp)|
|Spacts|Spacts or Special Actions are unlockable techniques that characters gain after leveling skills or stats to certain levels. Some of them are melee attacks with additional attributes or calculations. Some function more like spells. Others have effects that neither attack nor heal.|

You can eventually find the actual calculations involved as well as tables of weapontypes, spells, and spacts in the [Crunchdoc](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md).

### Additional Gameplay Elements that are Elaborated on in the Crunchdoc

- Reincarnation
- [Punishments for dying (stat loss, fame loss, curses/wounds that don't heal)](https://gitlab.com/SandwichChef/Demiplane/-/blob/dev/crunch.md#death-penalties)
- Minigames
- Gathering resources and crafting crafting crafting 
- Summoning gacha (one for randarts, one for pets)
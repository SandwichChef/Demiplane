# Crunch

## Gameplay

### Interacting with the environment inside of dungeons/towns/maps 

All of these things will be tied to a button, and will probably be some of the first things we implement in order for the basegame to be feature complete.

|Type of interaction|Notes| 
|---|---|
|Collision|If you step in or collide with certain types of environment hazards (lava, spikes, etc.) you can take damage or inventory damage|
|Bash/Kick|You can bash trees to make them drop fruit, or break other things such as doors/windows|
|Eat|You can eat berries/fruits/items off the floor or off of plants/trees|
|Dig|You can dig through solid tiles|
|Search|You can search on tiles to find traps and hidden passages, or possibly spot invisible enemies|
|Pick Up/Steal|You can pick up items that are on tiles, or steal them (uses/trains your pickpocket skill)|

### Pets

Pets and interacting with pets constitute one of the fun vectors for our game. As such, we're going to have to pay great attention to what you can/can't do with them, and all the various ways you can interact with them.

This table features a comprehensive list of our ideas so far.

|Pet Mechanic|Notes|
|---|---|
|Hiring (Pet acquisition)|I'm thinking that players should start with 1 pet based on their deity of choice, and then be given a choice of a starter pet at the first town. Afterwards, pets can be acquired by purchasing them from kennels, slave dealers, or by using resources to "summon" them (this will be like a psuedo-gacha)|
|Taming (Conversing with enemies/dæmons)|In addition to regular methods of pet acquisition, you can also try and convince enemies to join your party, or simply impress it upon them utilizing different skills or mechanics depending on the sort of enemy. For bestial enemies, perhaps you could just feed them or charm them. For dæmonic or humanoid enemies, you could converse with them and see what's up with them. Alternatively, you could mindbreak them.|
|Fusion|This is probably going to be one of the most complex aspects of the game. You can combine monsters to get better ones. Some are just fodder that contribute stats/skills/body parts. Some are unfusable. (So 3 categories, fusable, unfusable, fodder)|
|Cosmetic Customization|Just like players, I'm thinking you can 'upload' your own sprites for pets, or customize their sprites from available 'parts'. Additionally, you can customize their dialogue and have them say certain things in certain situation or when using certain spells/spacts.|
|Affection/Marriage|Interacting with your pet in a pleasant manner will increase their affection, and once their affection reaches max you can marry them. Additionally, I'm thinking that affection levels will confer bonuses, and also unlock additional ways you can interact with a given pet.|
|Breeding|You can have two pets breed to produce offspring, or you can breed with one yourself to create a child. You can continue playing the game as your child as a sort of NG+, or use a gene file resultant of coitus to start a new game.|
|Disassembly|You can alchemically break pets down to get raw materials, including a particular material necessary for pet summoning. This might incur a karma penalty, I'm not sure yet. There will probably be an achievement for disassembling a spouse.|
|Selling|You can sell your pets to slave markets for hella dosh.|
|Gambit System|You can program a Pet's AI using gambits, which are a set of conditionals you can select from.|
|Pet Arena (offline)|You can have your Pets fight in an in-game arena for money/fame/leveling, and the resource necessary for pet summoning|
|Pet Arena (online)|You can have your Pets fight in an online arena for the same stuff, but also for a spot on a leaderboard. The higher you rank, the more resources you get for summoning|


## Locations

### The Tower

Ascending this tower will probably be endgame content. I'm thinking once you reach the 100th floor or something you'll have a chance to enter the Chamber of Reflection where you'll be tested by Abraxas. If you succeed that'll count as an "ascension" and you'll have beaten the game. But that doesn't mean the game is over, because the game is a sandbox. Afterwards the tower will become endless and will scale linearly.

### The Fountain

This will function like the Truce Ground in Elona. There will be a fountain you can make wishes at (if you manage to acquire a rare resource). And shrines to all of the gods where you can change your religion or make offerings at.

### The Cumberland Mines

A seemingly endless network of mines running through and beneathe a magical series of mountains. 

### Fennario

A large country/region with a lot of towns and dungeons in it. In particular the Timbers of Fennario are where the wolffolk are from. One of the first quests will probably be fighting a Dire Wolf(girl) here. I'm thinking this will be the starting region, and the puppycave equivalent will likely be here.

## Combat

### Spellcasting

As of right now we've reckoned about 5 different ways to cast a spell, each with their own advantages/disadvantages

|Method|Notes|
|---|---|
|From a book|If you don't know the spell, you can use book charges to cast it, if you do know the spell you can cast it from the book with a higher mana cost but greatly increased damage dice/efficacy|
|From stock (gained from reading books)|This is the normal way to cast spells, normal mana cost, normal damage/efficacy, consumes stock.|
|From memory (gained from experience/training, once your spell reaches a certain level you can cast it from memory)|This is the other normal way to cast spells, slightly higher mana cost, and normal damage/efficacy. I'm thinking maybe you could consume stock if you wanted to in order to increase the efficacy/damage|
|From a scroll|The scroll is consumed in the process. When casting spells from scrolls and rods, damage is based on the level of the item, and the skill used to read/zap it|
|From a rod|The rod has charges, and using it consumes charges. They're infinitely rechargable but always have a chance to break. When casting spells from scrolls and rods, damage is based on the level of the item, and the skill used to read/zap it|

Instadeath seems like it might be a thing. Darkness element spells will have a chance to proc it. Chaos element spells will have a much lesser chance to proc it (because Chaos will have a chance to inflict all status ailments, of which instadeath is one)

### Spell Ideas

|Spell|Notes|
|---|---|
|Starfall|Like Meteor, but much weaker. Think of magic dart, but completely mapwide. Will likely have a version for most elements.|
|Magic Mapping|Spell that identifies all of the map, finds stairs/chests/traps and possibly monsters|
|Banishment (or something like it)|Spell that attempts to kill all of the enemies on a given floor. Will likely have a cost with a single die (i.e; 1d3) for each target.|

### Skill Ideas

## Death Penalties

Permadeath isn't really fun in a game with a whole lot of stuff to do in it. Though we might give players a permadeath option anyways. Instead, we reckon there should be death penalties. I was thinking that they should be thematically meaningful based on how you die.

|Cause of Death|Resultant Penalty|
|---|---|
|Psych Damage|Revive with less sanity, possibly forget a spell or lose skill points|
|Chaos Damage|Revive with less sanity, curse of the rainbow (reduced all resists, randomly be inflicted with status ailments)|
|Darkness Damage|Revive with less sanity, curse of the darkness (reduced Light and Dark resistance, do more damage to enemies, take more damage from enemies|
|Light Damage|Revive with less sanity, curse of the illuminated (reduced Light and Dark resistance, do less damage, all hit dice are decreased.|
|Fire Damage|Lose inventory items, curse of the flame (reduced fire resistance, randomly take damage over time)|
|Cold Damage|Lose inventory items, curse of the frost (reduced ice resistance, randomly take damage over time|
|Instadeath|You lose fame for getting owned in a bullshit manner|
|-|-|
|-|-|
|-|-|
|Non-elemental Damage|No penalty|
